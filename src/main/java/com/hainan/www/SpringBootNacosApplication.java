package com.hainan.www;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;

/**
 * @author 22372
 */
@SpringBootApplication
@NacosPropertySource(dataId = "haiguan", autoRefreshed = true)
@NacosPropertySource(dataId = "redis", autoRefreshed = true)
public class SpringBootNacosApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootNacosApplication.class, args);
    }

}

