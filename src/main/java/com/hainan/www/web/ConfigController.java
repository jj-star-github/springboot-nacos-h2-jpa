package com.hainan.www.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.hainan.www.annotation.Log;
import com.hainan.www.common.enums.BusinessType;

@RequestMapping("config")
@RestController
public class ConfigController {

	@NacosValue(value = "${useLocalCache:false}", autoRefreshed = true)
    private boolean useLocalCache;

	@NacosValue(value = "${dataCode:2021010}", autoRefreshed = true)
	private String dataCode;
	
	@NacosValue(value = "${redisPort:6673}", autoRefreshed = true)
	private String redisPort;
	
	@NacosValue(value = "${spring.qq.name:6673}", autoRefreshed = true)
	private String springApplicationName;
	
	@NacosValue(value = "${spring.redis.host:6673}", autoRefreshed = true)
	private String springRedisHost;
	
    @GetMapping("/get")
    @Log(title = "动态获取Nacos配置信息", businessType = BusinessType.INSERT)
    @ResponseBody
    public boolean get() {
    	System.out.println("useLocalCache==>" + useLocalCache);
    	
    	System.out.println("dataCode=>>>>>>" + dataCode);
    	
    	System.out.println("redisPort=>>>>>>" + redisPort);
    	
    	
    	System.out.println("springApplicationName=>>>>>>" + springApplicationName);
    	
    	System.out.println("springRedisHost=>>>>>>" + springRedisHost);
        return useLocalCache;
    }
}
