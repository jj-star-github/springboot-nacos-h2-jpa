package com.hainan.www.web;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hainan.www.entity.SysOperLog;

public interface SysOperLogRepository extends JpaRepository<SysOperLog, Integer> {
	
}